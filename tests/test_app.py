from vorlesung.app import app, summe, crazy_multiply


def test_summe():
    assert summe(1, 3) == 4


def test_crazy_multiply():
    assert crazy_multiply(2, 3) == 2
    assert crazy_multiply(2, 4) == 2
    assert crazy_multiply(3, 3) == 9


def test_hello_world():
    with app.test_client() as test_app:
        response = test_app.get("/")
        assert response.status_code == 200
        assert response.json == {
            "msg": "Hello World!"
        }
        # assert response.data.decode() == "{'msg':'Hello World!'}"


#def test_list_route():
#    with app.test_client() as test_app:
#        response = test_app.get("/list")
#        assert response.json == ["Hello", "World!", "random"]

#        response = test_app.get("/list")
#        assert response.json == ["Hello", "World!", "random", "random"]


def test_not_found():
    with app.test_client() as test_app:
        response = test_app.get("/notfound")
        assert response.status_code == 404
