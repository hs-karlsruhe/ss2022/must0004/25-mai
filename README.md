# 25-mai

## Prepare for pytest

```
pipenv install --dev pytest pytest-cov
```

## Run pytest with code coverage analysis

```
pytest --cov=vorlesung --cov-report=term-missing tests/
```
